﻿using System.Collections;
using System.Collections.Generic; 
using UnityEngine.EventSystems;
using UnityEngine;

public class AreaTap : MonoBehaviour, IPointerDownHandler {
    public void OnPointerDown(PointerEventData eventData) {
        GameManager.Instance.CollectByTap(eventData.position, transform);
    }

}
